# RMM Sample Database Sakila

Base de dados Sakila v.1.2 para uso como modelo para testes de outras tecnologias 

https://dev.mysql.com/doc/sakila/en

# Referências
## Tabelas

Tabelas disponíveis (em ordem alfabética)

- actor
- address
- category
- city
- country
- customer
- film
- film_actor
- film_category
- film_text
- inventory
- language
- payment
- rental
- staff
- store

# Esquemas

Visão geral EER 
![Imagem](/docs/images/sakila-er.png 'Esquema do Banco de dados Sakila')

Visão geral EER Documentada pelo Workbench
![Imagem](/docs/images/wb-sakila-eer.png 'Esquema do Banco de dados Sakila')

Visão minimizada
![Imagem](/docs/images/sakila-schema.png 'Esquema do Banco de dados Sakila')

# Manual
- [Manual](/docs/sakila-en.pdf)

# Atualizações

# Usando container Docker

## ajustando configurações
Você pode alterar o arquivo de parâmetros para as suas necessidades
``` bash
./config/dev.env
```

https://docs.docker.com/compose/environment-variables/


## verificando a configuração de acordo com o environment escolhido
``` bash
host> docker-compose --env-file ./config/.env.dev config 
```

## Regenerando
Acrescente 
``` bash
--build
```

## Iniciando de acordo com o environment escolhido
``` bash
host> docker-compose --env-file ./config/.env.dev up
```

## Background
Para carregamento em segundo plano acrescente "-d": 

``` bash
host> docker-compose --env-file ./config/.env.dev up -d
```




Primeiro rodar

build.sh

up.sh