#/bin/bash

# Ricardo Melo Martins
# 
# Referencias
# https://docs.docker.com/engine/reference/commandline/build/

echo "docker remove"

docker container ls -a

docker container stop $(docker container ls -aq)

docker container rm db_mysql -v

docker container ls -a

docker image ls