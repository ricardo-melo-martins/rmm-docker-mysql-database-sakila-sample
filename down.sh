#/bin/bash

# Ricardo Melo Martins
#
# Referencias
# https://docs.docker.com/compose/reference/down/

echo "docker-compose down"

docker-compose down --volumes

# docker volume prune -f

docker ps