#/bin/bash

# Ricardo Melo Martins
#
# Referencias
# https://docs.docker.com/compose/reference/up/

echo "docker-compose up"

# docker-compose -f ./docker-compose.yml up -d --build mysql
docker-compose -f ./docker-compose.yml up -d 

docker ps